ARG RUBY_VERSION=3.0

FROM ruby:${RUBY_VERSION}

RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get update -qq && apt-get install -y -qq nodejs
RUN gem install bundler
RUN npm install -g yarn
RUN ruby -v
RUN which ruby
RUN bundler -v
RUN yarn -v
